#!/bin/bash 

#######################################################################
#Script Name    : delete_instance.sh
#Description    : Delete an chameleon instance along with lease used.
#                 Installs packages required using `pipenv install` 
#                 and runs the required python scripts using `pipenv run`.    
#Args           : OS Region of Chameleon Cloud (CHI@UC or CHI@TACC),
#                 Name of lease to be deleted,
#                 Appliance/Instance Name to be deleted
#Author         : Sridutt Bhalachandra                                                
#Email          : sriduttb@anl.gov                                           
#######################################################################

SCRIPT_PATH=`pwd`
#PIPENV_PATH=`which pipenv`

# Set environment variable for pipenv
export OS_REGION_NAME=$1

# chi-lease parameters
lease_name=$2

# chi-appliance parameters
app_name=$3

if pipenv run python -c "import blazarclient" &> /dev/null; then
    echo "Required modules are installed."
else
    echo "Modules not installed. Installing now..."
    pipenv install
    echo "Checking modules are loaded by verifying your credentials..."
    pipenv run ./chi-mngmt.py list-keys
    echo "On successful, you will be able to see your SSH pubic key, including its name"
fi

echo "Deleting appliance/instance required..."
pipenv run $SCRIPT_PATH/chi-appliance.py delete $app_name 
echo "Deleted appliance/instance"

echo "Deleting lease required..."
pipenv run $SCRIPT_PATH/chi-lease.py delete $lease_name 
echo "Deleted lease."
