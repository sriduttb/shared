#!/bin/bash 

#######################################################################
#Script Name    : create_instance.sh
#Description    : Create an chameleon instance with specified number of
#                 nodes for the specified period. Also, creates the    
#                 lease required prior to creating an instance
#                 lease required prior to creating an instance. Installs                    
#                 packages required using `pipenv install` and runs the                      
#                 required python scripts using `pipenv run`.    
#Args           : OS Region of Chameleon Cloud (CHI@UC or CHI@TACC),
#                 Name of lease to be created,
#                 Number of Nodes,
#                 Number of Hours,
#                 Appliance/Instance Name to be created,
#                 Optionally specify 'haswell' for haswell node,
#                 OS Image to be deployed (optional after assignment),
#                 SSH Key Name to be used (optional after assignment),
#                 Appliance template file (optional after assignment)
#Author         : Sridutt Bhalachandra                                                
#Email          : sriduttb@anl.gov                                           
#######################################################################

SCRIPT_PATH=`pwd`
#PIPENV_PATH=`which pipenv`

# Set environment variable for pipenv
export OS_REGION_NAME=$1

# chi-lease parameters
lease_name=$2
num_nodes=$3
num_hours=$4

# chi-appliance parameters
app_name=$5
node_type=$6
os_image=${7:-'CC-CentOS7'}
ssh_key=${8:-'sb_ssh_pub'}
app_template_file=${9:-'chameleon-appliances/bare-template.yaml'}

if pipenv run python -c "import blazarclient" &> /dev/null; then
    echo "Required modules are installed."
else
    echo "Modules not installed. Installing now..."
    pipenv install
    echo "Checking modules are loaded by verifying your credentials..."
    pipenv run ./chi-mngmt.py list-keys
    echo "On successful, you will be able to see your SSH pubic key, including its name"
fi

echo "Deleting old lease with same name..."
pipenv run $SCRIPT_PATH/chi-lease.py delete $lease_name 
echo "Deleted old lease."

echo "Creating new lease..."
if [ "$node_type" = "haswell" ]; then
    pipenv run $SCRIPT_PATH/chi-lease.py create --haswell $lease_name $num_nodes $num_hours
else
    pipenv run $SCRIPT_PATH/chi-lease.py create $lease_name $num_nodes $num_hours
fi
echo "Created new lease."

echo "Deleting old appliance with same name..."
pipenv run $SCRIPT_PATH/chi-appliance.py delete $app_name 
echo "Deleted old appliance."

echo "Creating Appliance configuration file with specified OS Image using
template file..."
sed 's/CC-CentOS7/'"$os_image"'/g' $app_template_file > $SCRIPT_PATH/temp_AppConfig.yaml
echo "Created Appliance configuration file."

echo "Creating new appliance instance..."
pipenv run $SCRIPT_PATH/chi-appliance.py create --wait $app_name $lease_name $SCRIPT_PATH/temp_AppConfig.yaml '{"key_name":"'"$ssh_key"'"}'
echo "Created new appliance instance."

rm $SCRIPT_PATH/temp_AppConfig.yaml
echo "Appliance configuration file deleted."
